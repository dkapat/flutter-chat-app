import 'package:firebase_auth/firebase_auth.dart';

import '../services/auth.dart';
import '../services/database.dart';
import '../widgets/widget.dart';
import 'package:flutter/material.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  AuthMethods authMethods = new AuthMethods();
  DatabaseMethods databaseMethods = new DatabaseMethods();

  final formKey = GlobalKey<FormState>();

  TextEditingController emailTextEditingController = new TextEditingController();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  resetMyPassword(){
    authMethods.resetPass(emailTextEditingController.text).then((value){
      final snackBar = new SnackBar(
        content: new Text("Check your email"),
        duration: new Duration(seconds: 3),
      );
      _scaffoldKey.currentState.showSnackBar(snackBar);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: appBarMain(context),
      body: Center(
        child: Padding(
          padding: EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Text(
                "We will mail you a link ... Please click on that link to reset your password.",
                style: TextStyle(color: Colors.white, fontSize: 20,),
              ),
              SizedBox(height: 20,),
              Form(
                key: formKey,
                child: Column(
                  children: <Widget>[
                    TextFormField(
                        validator: (val){
                          return RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(val) ? null : "Enter Correct Email";
                        },
                        controller: emailTextEditingController,
                        style: simpleTextStyle(),
                        decoration: textFieldInputDecoration("Enter Your Email")
                    ),
                  ],
                ),
              ),
              SizedBox(height: 20,),
              GestureDetector(
                onTap: (){
                  resetMyPassword();
                },
                child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.symmetric(vertical: 20),
                  decoration: BoxDecoration(
                    gradient: LinearGradient(colors: [
                      const Color(0xff007EF4),
                      const Color(0xff2A75BC),
                    ]),
                    borderRadius: BorderRadius.circular(30),
                  ),
                  child: Text("Send Email", style: mediumTextStyle(),),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}